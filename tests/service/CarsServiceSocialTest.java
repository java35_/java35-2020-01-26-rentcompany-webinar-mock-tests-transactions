package service;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import db.embedded.RentCompanyEmbedded;
import db.entities.CarEntity;
import db.entities.ModelEntity;
import db.entities.State;

public class CarsServiceSocialTest {
	
	private static final String CAR_REG_NUMBER_EXISTS = "carRegNumber1";
	private static final String CAR_REG_NUMBER_NOT_EXISTS = "carRegNumber2";
	private static final CarEntity CAR_EXISTS = 
			new CarEntity(CAR_REG_NUMBER_EXISTS, "white", State.EXCELENT, 
							"modelName1", false, false);
	private static final CarEntity CAR_NOT_EXISTS = 
			new CarEntity(CAR_REG_NUMBER_NOT_EXISTS, "white", State.EXCELENT, 
							"modelName1", false, false);
	final ModelEntity MODEL_EXISTS = new ModelEntity("modelName1", 60, "company1", 
														"country1", 100);
	
	CarsService service;
	RentCompanyEmbedded db;
	
	// JUnit4 - @Before
	@BeforeEach
	void setUp() {
		db = new RentCompanyEmbedded();
		service = new CarsService(db);
		db.saveModel(MODEL_EXISTS);
		db.saveCar(CAR_EXISTS);
	}
	
	@Test
	void findCarById() {
		assertEquals(CAR_EXISTS, service.findCarById(CAR_REG_NUMBER_EXISTS));
		assertNull(service.findCarById(CAR_REG_NUMBER_NOT_EXISTS));
		assertNull(service.findCarById(""));
		assertNull(service.findCarById(null));
	}
	
	@Test
	void deleteCarById() {
		Short expected; // ���������
		Short actual; // ����������, �������
//		assertEquals(true, service.deleteCarById(CAR_REG_NUMBER_EXISTS));
//		assertEquals(true, service.deleteCarById(CAR_REG_NUMBER_NOT_EXISTS));
//		assertEquals(false, service.deleteCarById(""));
//		assertEquals(false, service.deleteCarById(null));
		
		assertTrue(service.deleteCarById(CAR_REG_NUMBER_EXISTS));
		assertTrue(service.deleteCarById(CAR_REG_NUMBER_NOT_EXISTS));
		assertFalse(service.deleteCarById(""));
		assertFalse(service.deleteCarById(null));
	}
}
