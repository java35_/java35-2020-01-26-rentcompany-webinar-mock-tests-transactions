package db.entities;

import java.time.LocalDate;

public class DriverEntity {
	int licenseId;
	String name;
	LocalDate birthDate;
	String phone;
	
	public DriverEntity(DriverEntity driver) {
		this.licenseId 	= driver.licenseId;
		this.name		= driver.name;
		this.birthDate 	= driver.birthDate;
		this.phone 		= driver.phone;
	}
	
	
	public DriverEntity(int licenseId, String name, LocalDate birthDate, String phone) {
		this.licenseId = licenseId;
		this.name = name;
		this.birthDate = birthDate;
		this.phone = phone;
	}


	public int getLicenseId() {
		return licenseId;
	}
}
