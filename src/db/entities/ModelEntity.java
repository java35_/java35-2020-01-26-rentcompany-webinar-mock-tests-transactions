package db.entities;

public class ModelEntity {
	String modelName;
	int gasTank;
	String company;
	String country;
	int priceDay;
	@Override
	public String toString() {
		return "ModelEntity [modelName=" + modelName + ", gasTank=" + gasTank + ", company=" + company + ", country="
				+ country + ", priceDay=" + priceDay + "]";
	}
	public ModelEntity(String modelName, int gasTank, String company, String country, int priceDay) {
		this.modelName = modelName;
		this.gasTank = gasTank;
		this.company = company;
		this.country = country;
		this.priceDay = priceDay;
	}
	public ModelEntity(ModelEntity entity) {
		this.modelName = entity.modelName;
		this.gasTank = entity.gasTank;
		this.company = entity.company;
		this.country = entity.country;
		this.priceDay = entity.priceDay;
	}
	public String getModelName() {
		return modelName;
	}
	public int getGasTank() {
		return gasTank;
	}
	public String getCompany() {
		return company;
	}
	public String getCountry() {
		return country;
	}
	public int getPriceDay() {
		return priceDay;
	}
}
