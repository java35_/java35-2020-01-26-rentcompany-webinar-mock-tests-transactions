package service;

import db.embedded.RentCompanyEmbedded;
import db.entities.CarEntity;

public class CarsService {
	
	RentCompanyEmbedded db;
	
	public CarsService (RentCompanyEmbedded db) {
		if (db == null)
			db = new RentCompanyEmbedded();
		this.db = db;
	}
	
	public CarEntity findCarById(String regNumber) {
		if (regNumber == null || regNumber.isBlank())
			return null;
		CarEntity carEntity = db.findCarById(regNumber);
		if (carEntity == null)
			return null;
		return carEntity.isFlRemoved() ? null : carEntity;
	}
	
	// hard delete 
	public boolean deleteCarById(String regNumber) {
		if (regNumber == null || regNumber.isBlank())
			return false;
		db.deleteCarById(regNumber);
		return true;
	}
	
	// Soft delete
	public boolean deleteSoftCarById(String regNumber) {
		if (regNumber == null || regNumber.isBlank())
			return false;
		CarEntity carEntity = db.findCarById(regNumber);
		if (carEntity == null)
			return false;
		carEntity.setFlRemoved(true);
		return db.saveCar(carEntity) == null ? false : true;
	}
	
//	private CarEntity getCarByIdPrivate(String regNumber) {
//		assert regNumber != null && !regNumber.isBlank();
//	}
}
